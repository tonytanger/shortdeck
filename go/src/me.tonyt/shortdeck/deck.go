package shortdeck

import (
	"errors"
	"math/rand"
	"sort"
	"time"
)

type Suit int

const (
	Spade Suit = iota
	Heart
	Club
	Diamond
)

var SuitMatch = []string{"s", "h", "c", "d"}

func (s Suit) String() string {
	return SuitMatch[s]
}

type Value int

const (
	Six Value = iota
	Seven
	Eight
	Nine
	Ten
	Jack
	Queen
	King
	Ace
)

var ValueMatch = []string{"6","7","8","9","T","J","Q","K","A"}

func (s Value) String() string {
	return ValueMatch[s]
}

type Card struct {
	Value Value
	Suit Suit
	probability float32
}

func ParseCard(handString string) (Card, error) {
	if len(handString) != 2 {
		return Card{}, errors.New("Wrong length for hand being parsed")
	}
	var value Value
	var suit Suit
	foundValue, foundSuit := false, false
	for i := range ValueMatch {
		if ValueMatch[i] == string(handString[0]) {
			value = Value(i)
			foundValue = true
		}
	}
	for i := range SuitMatch {
		if SuitMatch[i] == string(handString[1]) {
			suit = Suit(i)
			foundSuit = true
		}
	}
	if foundValue && foundSuit {
		return Card{value, suit, 0}, nil
	}
	return Card{}, errors.New("Invalid hand string")
}

type ShortDeck interface {
	Shuffle()
	DealOneCard() Card
	RemoveCard(card Card)
}

type shortDeck struct{
	cards []Card
}

func randomNewCards() []Card {
	v := []Value{Ace, Six, Seven, Eight, Nine, Ten, Jack, Queen, King}
	s := []Suit{Spade, Heart, Club, Diamond}
	var cards []Card
	rand.Seed(time.Now().UTC().UnixNano())
	for _, value := range v {
		for _, suit := range s {
			cards = append(cards, Card{value, suit, rand.Float32()})
		}
	}
	sort.Slice(cards, func(i, j int) bool { return cards[i].probability < cards[j].probability})
	return cards
}

func NewShortDeck() ShortDeck {
	return &shortDeck{randomNewCards()}
}

func (d *shortDeck) Shuffle() {
	d.cards = randomNewCards()
}

func (d *shortDeck) DealOneCard() Card {
	if len(d.cards) == 0 {
		return Card{}
	}
	var card Card
	card, d.cards = d.cards[0], d.cards[1:]
	return card
}

func (d *shortDeck) RemoveCard(card Card) {
	for i := range d.cards {
		if d.cards[i].Value == card.Value && d.cards[i].Suit == card.Suit {
			d.cards = append(d.cards[:i], d.cards[i+1:]...)
		}
	}
}

