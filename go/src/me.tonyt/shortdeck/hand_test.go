package shortdeck

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSort(t *testing.T) {
	card1, _ := ParseCard("Ac")
	card2, _ := ParseCard("Td")
	card3, _ := ParseCard("4s")
	card4, _ := ParseCard("Ah")
	card5, _ := ParseCard("As")
	hand := Hand{Cards: []Card{card1, card2, card3, card4, card5}}

	assert.False(t, hand.sorted)
	hand.Sort()
	assert.True(t, hand.sorted)
	fmt.Print(hand.Cards)
}

func TestHand_HasThreeOfAKind(t *testing.T) {
	card1, _ := ParseCard("Ac")
	card2, _ := ParseCard("Td")
	card3, _ := ParseCard("6s")
	card4, _ := ParseCard("Ah")
	card5, _ := ParseCard("As")
	hand := Hand{Cards: []Card{card1, card2, card3, card4, card5}}

	assert.False(t, hand.sorted)
	hand.Sort()
	assert.True(t, hand.sorted)
	fmt.Print(hand.Cards)
	assert.True(t, hand.HasThreeOfAKind())
	assert.False(t, hand.HasFourOfAKind())
}

func TestHand_HasFourOfAKind(t *testing.T) {
	card1, _ := ParseCard("Ac")
	card2, _ := ParseCard("Ad")
	card3, _ := ParseCard("6s")
	card4, _ := ParseCard("Ah")
	card5, _ := ParseCard("As")
	hand := Hand{Cards: []Card{card1, card2, card3, card4, card5}}

	assert.False(t, hand.sorted)
	hand.Sort()
	assert.True(t, hand.sorted)
	fmt.Print(hand.Cards)
	assert.True(t, hand.HasThreeOfAKind())
	assert.True(t, hand.HasFourOfAKind())
}

func TestHand_HasStraight(t *testing.T) {
	card1, _ := ParseCard("Ac")
	card2, _ := ParseCard("Kd")
	card3, _ := ParseCard("Ts")
	card4, _ := ParseCard("Qh")
	card5, _ := ParseCard("Js")
	hand := Hand{Cards: []Card{card1, card2, card3, card4, card5}}

	assert.False(t, hand.sorted)
	hand.Sort()
	assert.True(t, hand.sorted)
	fmt.Print(hand.Cards)
	assert.True(t, hand.HasStraight())
}