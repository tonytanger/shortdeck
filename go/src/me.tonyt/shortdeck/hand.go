package shortdeck

import "sort"

type HandQuality int

const (
	HighCard HandQuality = iota
	OnePair
	TwoPair
	Straight
	ThreeOfAKind
	FullHouse
	Flush
	FourOfAKind
	StraightFlush
	RoyalFlush
)

type HandMade struct {
	handQuality HandQuality
	ordering []Value
}

type Hand struct {
	sorted bool
	Cards  []Card
}

func (h *Hand) Sort() {

	sort.Slice(h.Cards, func(i, j int) bool { return int(h.Cards[i].Value) < int(h.Cards[j].Value) })
	h.sorted = true
}

func (h *Hand) Pairs() int {
	pairs := 0
	for start, end := 0, 1; end < len(h.Cards); func() { end++; start++ }() {
		if h.Cards[start].Value == h.Cards[end].Value {
			pairs++
			start++
			end++
		}
	}
	return pairs
}


func (h *Hand) HasStraight() bool {
	if h.Cards[len(h.Cards) - 1].Value == Ace && h.Cards[0].Value == Six {
		// check for wheel
		for i := 0; i < len(h.Cards) - 2; i++ {
			if h.Cards[i].Value + 1 != h.Cards[i+1].Value {
				return false
			}
		}
		return true
	}
	for i := 0; i < len(h.Cards) - 1; i++ {
		if h.Cards[i].Value + 1 != h.Cards[i+1].Value {
			return false
		}
	}
	return true
}

func (h *Hand) HasThreeOfAKind() bool {
	for start, end := 0, 2; end < len(h.Cards); func() { end++; start++ }() {
		if h.Cards[start].Value == h.Cards[end].Value {
			return true
		}
	}
	return false
}



func (h *Hand) HasFourOfAKind() bool {
	for start, end := 0, 3; end < len(h.Cards); func() { end++; start++ }(){
		if h.Cards[start].Value == h.Cards[end].Value {
			return true
		}
	}
	return false
}

func (h *Hand) Strength() {

}